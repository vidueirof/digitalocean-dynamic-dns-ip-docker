# digitalocean-dynamic-dns-ip-docker

Image based on https://github.com/anaganisk/digitalocean-dynamic-dns-ip

## Usage

Create a file `digitalocean-dynamic-ip.json`. Add the following JSON:

```json
{
  "apikey": "samplekeydasjkdhaskjdhrwofihsamplekey",
  "doPageSize": 20,
  "useIPv4": true,
  "useIPv6": false,
  "allowIPv4InIPv6": false,
  "ipv4CheckUrl": "https://ipv4bot.whatismyipaddress.com",
  "domains": [
    {
      "domain": "example.com",
      "records": [
        {
          "name": "subdomainOrRecord",
          "type": "A"
        }
      ]
    },
    {
      "domain": "example2.com",
      "records": [
        {
          "name": "subdomainOrRecord2",
          "type": "A",
          "TTL": 30
        }
      ]
    }
  ]
}
```

Run:
```bash
docker run -v digitalocean-dynamic-ip.json:/root/.digitalocean-dynamic-ip.json vidueirof/digitalocean-dynamic-dns-ip
```
