# Stage 0, "build-stage", based on Go, to build and compile the frontend
FROM golang:1.11 as build-stage
WORKDIR /app

RUN git clone https://github.com/anaganisk/digitalocean-dynamic-dns-ip.git /app

# build the project
RUN go build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM debian:stable

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
    && apt-get purge -y \
    && apt-get clean -y \
    && apt-get autoremove -y \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*

#Copy ci-dashboard-dist
COPY --from=build-stage /app/digitalocean-dynamic-dns-ip /usr/local/bin

CMD [ "digitalocean-dynamic-dns-ip" ]